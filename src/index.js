import React from 'react';
import {render} from 'react-dom';
import { Provider } from 'react-redux';
import { Router, browserHistory } from 'react-router';
import {createStore, applyMiddleware,compose} from 'redux'
import thunk from 'redux-thunk';
import routes from './routes';
import rootReducer from '../src/reducers/index';
import configureStore from './store/configureStore';
import {loadPlaces} from './actions/ActionsPlace';

const store = configureStore();

store.dispatch(loadPlaces());


render(
	<Provider store={store}>
	<Router history={browserHistory} routes={routes}/>
	</Provider> , document.getElementById('app'));

