import * as types from './ActionTypes';
import placesApi from '../api/PlacesApi';

export function loadPlacesSuccess(places) {
  return {type: types.LOAD_PLACES_SUCCESS, places};
}

export function updatePlaceSuccess(place) {
  return {type: types.UPDATE_PLACE_SUCCESS, place}
}

export function createPlaceSuccess(place) {
  return {type: types.CREATE_PLACE_SUCCESS, place}
}

export function deletePlaceSuccess(place) {
  return {type: types.DELETE_PLACE_SUCCESS, place}
}

export function loadPlaces() {
  return function(dispatch) {
    return placesApi.getAllPlaces().then(places => {
      dispatch(loadPlacesSuccess(places));
    }).catch(error => {
      throw(error);
    });
  };
}

export function updatePlace(place) {
  return function (dispatch) {
    return placesApi.updatePlace(place).then(responsePlace => {
      dispatch(updatePlaceSuccess(responsePlace));
    }).catch(error => {
      throw(error);
    });
  };
}

export function createPlace(place) {
  return function (dispatch) {
    return placesApi.createPlace(place).then(responsePlace => {
      dispatch(createPlaceSuccess(responsePlace));
      return responsePlace;
    }).catch(error => {
      throw(error);
    });
  };
}

export function deletePlace(place) {
  return function(dispatch) {
    return placesApi.deletePlace(place).then(() => {
      dispatch(deletePlaceSuccess(place));
      return;
    }).catch(error => {
      throw(error);
    })
  }
}







