import { browserHistory } from 'react-router';
import * as types from './ActionTypes';
import userApi from '../api/UserApi';
import { combineReducers } from 'redux';

export function createUserSuccess(user) {
  return { type: types.CREATE_USER_SUCCESS, user }
}

export function loadUsersSuccess(users) {

  return { type: types.LOGIN_USER_SUCCESS, users };
}

export function loginRequest(user) {
  return { type: types.LOGIN_REQUEST, user }
}

export function loginSuccess(user) {
  return { type: types.LOGIN_SUCCESS, user }
}

export function loginFailure(error) {
  return { type: types.LOGIN_FAILURE, error }
}

export function logoutSuccess() {
  return { type: types.LOGOUT_SUCCESS }
}

export function alertSuccess(message) {
  return { type: types.ALERT_SUCCESS, message };
}

export function alertError(message) {
  return { type: types.ALERT_ERROR, message };
}

export function alertClear() {
  return { type: types.ALERT_CLEAR };
}

export function loadUsers() {
  return function (dispatch) {
    return userApi.getAllUsers().then(users => {
      dispatch(loadUsersSuccess(users));
    }).catch(error => {
      throw (error);
    });
  };
}

export function createUser(user) {
  return function (dispatch) {
    return userApi.createUser(user).then(responseUser => {
      dispatch(createUserSuccess(responseUser));
      return responseUser;
    }).catch(error => {
      throw (error);
    });
  };
}

export function loginUser(username, password) {
  return function (dispatch) {
    return userApi.login(username, password)
      .then((user) => {
        dispatch(loginSuccess(user));
        browserHistory.push('/userHome');
        return user;
      }).catch(error => {
        browserHistory.push('/login');
        throw (error);
      });
  }
}

export function logout() {
  return function (dispatch) {
    dispatch(logoutSuccess())
    return userApi.logout();
  }
}

export function logoutAdmin() {
  return function (dispatch) {
    dispatch(logoutSuccess())
    return userApi.logoutAdmin();
  }
}
