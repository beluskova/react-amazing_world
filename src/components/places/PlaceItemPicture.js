import React, {PropTypes, Component, ListItem} from 'react';
import {Link} from 'react-router';
import Place from './Place';
import CSS       from '../../../src/css/components/home.less';


const PlaceItemPicture = ({place}) => {
  return (
    <div className="gallery">
      <Link to={'/places/' + place.id}>
      <img className="beside" src={require('../../public/assets/images/' + place.imageName)}/>
      </Link>
      </div>
  );
}
PlaceItemPicture.propTypes = {
  place: PropTypes.object.isRequired
}

export default PlaceItemPicture;


