import React, {PropTypes, Component} from 'react';
import {Link} from 'react-router';
import Place from './Place';

const PlaceItem = ({place}) => {
  return (
    <tr>
      <td><Link to={'/places/' + place.id}>{place.name}</Link></td>
    </tr>
  );
}

PlaceItem.propTypes = {
  place: PropTypes.object.isRequired
}

export default PlaceItem;


