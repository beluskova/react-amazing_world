import React, {PropTypes} from 'react';
import PlaceItem from './PlaceItem';

const ListPlaceItems = ({places}) => {
  return (
    <table className="table">
      <thead>
        <tr>
         <th>Places:</th>
        </tr>
      </thead>
      <tbody>
        {places.map(place => 
          <PlaceItem key={place.id} place={place} />
        )}
      </tbody>
    </table>
  );
};

ListPlaceItems.propTypes = {
  places: PropTypes.array.isRequired
};

export default ListPlaceItems;