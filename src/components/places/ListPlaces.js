import React, {PropTypes} from 'react';
import {Link, browserHistory} from 'react-router';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as ActionsPlace from '../../actions/ActionsPlace';
import ListPlaceItems from './ListPlaceItems';
import PlaceItem from './PlaceItem';

class ListPlaces extends React.Component {
  constructor(props, context) {
    super(props, context);
  }

handleClick() {
   browserHistory.push('/newplace');
  }

render() {
  const userAdmin = JSON.parse(localStorage.getItem('userAdmin'));

  
const places = this.props.places;
return (
  <div className="app-container">
   {userAdmin ? 
      <button type="submit"  onClick={this.handleClick.bind(this)}> Add a new place</button> 
      :
      <p></p> 
      }
    <h1> Places </h1>
    <div className="col-md-4">
      <h2><ListPlaceItems places={places} /></h2>
    </div>
    <div className="col-md-8">
      {this.props.children}
    </div>
  </div>
);
}
}


ListPlaces.propTypes = {
  places: PropTypes.array.isRequired,
  children: PropTypes.object
};

function mapStateToProps(state, ownProps) {
  return {
    places: state.places
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(ActionsPlace, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ListPlaces);



