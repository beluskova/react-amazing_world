
import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as ActionsPlace from '../../actions/ActionsPlace';
import NewPlaceForm from './NewPlaceForm';
import {browserHistory} from 'react-router';
import anantaraOman from '../../public/assets/images/anantaraOman.jpg';
import canola from '../../public/assets/images/canolaChina.jpg';

class Place extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      place: Object.assign({}, this.props.place), 
      saving: false,
      isEditing: false
    };
    this.savePlace = this.savePlace.bind(this);
    this.updatePlaceState = this.updatePlaceState.bind(this);
    this.toggleEdit = this.toggleEdit.bind(this);
    this.deletePlace = this.deletePlace.bind(this);
    this.redirect = this.redirect.bind(this);
    console.log(this.state);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.place.id != nextProps.place.id) {
      this.setState({place: Object.assign({}, nextProps.place)});
    }
    this.setState({saving: false, isEditing: false});
  }

  toggleEdit() {
    this.setState({isEditing: true});
  }

  updatePlaceState(event) {
    const field = event.target.name;
    const place = this.state.place;
    place[field] = event.target.value;
    return this.setState({place: place});
  }

  savePlace(event) {
    event.preventDefault();
    this.setState({saving: true});
    this.props.actions.updatePlace(this.state.place);

  } 
  

  deletePlace(event) {
    this.props.actions.deletePlace(this.state.place)
    console.log("Place deleted");
  }

  redirect() {
    browserHistory.push('/places');
  }

  render() {
    const userAdmin = JSON.parse(localStorage.getItem('userAdmin'));

   if (this.state.isEditing) {
      return (
        <div className="basic-input">
        <h1>Edit place</h1>
        <NewPlaceForm 
          place={this.state.place} 
          onSave={this.savePlace} 
          onChange={this.updatePlaceState} 
          saving={this.state.saving}/> 
        </div>
      )
    }
    return (

      <div className="home">
      <h2> {this.props.place.name}</h2>
       <div className="col-md-5">
        <p><b> State: </b> {this.props.place.state}</p>
        <p><b> Type:</b> {this.state.place.type}</p>
        <p><b>Interesting facts:</b> {this.state.place.interesting}</p>
        <p> {this.state.place.description}</p>
        </div>
        <div className="col-md-5">
        <img className='picture' style={{'width': '400px', 'height': '500px'}} src={require('../../public/assets/images/' + this.props.place.imageName)}/>
        </div>
        <div className="col-md-12">
        {userAdmin ? 
          <button type="submit" onClick={this.toggleEdit} className="submit">Edit</button> :
          <p></p> }
        {userAdmin ? 
          <button type="submit" onClick={this.deletePlace} className="submit">Delete</button> :
         <p></p> }
      </div>
      </div>
    );
  }
}


Place.propTypes = {
  place: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired
};

function getPlaceById(places, id) {
  let place = places.find(place => place.id == id)
  console.log(id);
  console.log(place.id);
  return Object.assign({}, place)
}

function mapStateToProps(state, ownProps) {
  let place = {name: '', state: '', type: '', interesting: '', urls:''};
  const placeId = ownProps.params.id;
  if (placeId && state.places.length > 0) {
    place = getPlaceById(state.places, ownProps.params.id);
  } 
    return {place: place};
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(ActionsPlace, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Place);







