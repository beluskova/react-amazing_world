import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';  
import { registerUser } from '../../actions/ActionsUser';
import ClassNames from 'classnames';
import FormInput from '../partials/FormInput';
import { bindActionCreators } from 'redux';
import { browserHistory } from 'react-router';
import * as courseActions from '../../actions/ActionsUser';
import NewUserForm from './NewUserForm';
import userApi from '../../api/UserApi';
import CSS from '../../css/components/basicInput.less';

class Register extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      user: { username: '', firstname: '', lastname: '', email: '', password: '' },
      saving: false
    };
    this.redirect = this.redirect.bind(this);
    this.saveUser = this.saveUser.bind(this);
    this.updateUserState = this.updateUserState.bind(this);
  }

  updateUserState(event) {
    const field = event.target.name;
    const user = this.state.user;
    user[field] = event.target.value;
    return this.setState({ user: user });
  }

  redirect(user) {
    browserHistory.push('/userHome');
  }

  saveUser(event) {
    event.preventDefault();
    this.props.actions.createUser(this.state.user)
      .then((user) => {
        this.redirect(user);
      });
  }

  handleSubmit(event) {
    event.preventDefault();

    this.setState({ submitted: true });
    const { user } = this.state;
    const { dispatch } = this.props;
    if (user.firstName && user.lastName && user.username && user.password) {
      dispatch(userActions.register(user));
    }
  }

  render() {
    return (

      <div className="basic-input">
        <div className="login-box">
          <fieldset>
            <legend>Sign up here</legend>
            <NewUserForm
              user={this.state.user}
              onSave={this.saveUser}
              onChange={this.updateUserState} />
          </fieldset>
        </div>
      </div>
    );
  }
}


Register.propTypes = {
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state, ownProps) {
  return {
    user: state.user
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(courseActions, dispatch)
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(Register);





