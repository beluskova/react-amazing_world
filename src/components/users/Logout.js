import React,  {PropTypes}     from 'react';
import {Link} from 'react-router';;
import { History } from 'react-router';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as courseActions from '../../actions/ActionsUser';
import {browserHistory} from 'react-router';


class Logout extends React.Component {

  componentWillMount () {
    this.props.actions.logout();
    browserHistory.push('/');
}

render () {
    return null;
}
};

Logout.propTypes = {
  actions: PropTypes.object.isRequired

};

function mapStateToProps(state, ownProps) {
  return {
    user: state.user
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(courseActions, dispatch)
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(Logout);

