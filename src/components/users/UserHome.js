import React from 'react';
import {Link} from 'react-router';
import {browserHistory} from 'react-router';

class UserHome extends React.Component {

  render() {
    const user = JSON.parse(localStorage.getItem('user'));
    const userAdmin = JSON.parse(localStorage.getItem('userAdmin'));
 
  {if (user)  {
  return (
    <div>
    {user ?
     <h1> Welcome {user.firstname} on your home page! </h1> :
     userAdmin ?
     <h1> Welcome {userAdmin.firstname} on your home page! </h1> :
     <h1> Welcome on your home page! It should be impossible to reach here!!! </h1> }
    </div>
  )} 
  else {
    console.log("no token => no userHome");
    browserHistory.push('/');
    return null;
  }
  }
  }
}
export default UserHome;