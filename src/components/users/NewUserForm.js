import React, {PropTypes} from 'react';
import FormInput from '../partials/FormInput';
import ClassNames from 'classnames';


class NewUserForm extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className={ClassNames({ 'error': this.props.error})} {...this.props} >
        <form className="login-form" onSubmit={this.props.onSave}>
          <FormInput
            name="username"
            placeholder="username"
            value={this.props.user.username}
            onChange={this.props.onChange}/>

          <FormInput
            name="firstname"
            placeholder="first name"
            value={this.props.user.firstName}
            onChange={this.props.onChange}/>

          <FormInput
            name="lastname"
            placeholder="last name"
            value={this.props.user.lastname}
            onChange={this.props.onChange}/>

          <FormInput
            name="email"
            placeholder="email"
            value={this.props.user.email}
            onChange={this.props.onChange}/>

          <FormInput
            name="password"
            placeholder="password"
            type="password"
            value={this.props.user.password}
            onChange={this.props.onChange}/>
          <p> </p>
          <button type="submit"> submit </button>
        </form>
      </div>
  );
  }
}

NewUserForm.propTypes = {
  user: React.PropTypes.object.isRequired,
  onSave: React.PropTypes.func.isRequired,
  onChange: React.PropTypes.func.isRequired,

};

export default NewUserForm;
