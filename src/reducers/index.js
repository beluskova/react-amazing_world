import {combineReducers} from 'redux';
import places from './PlaceReducer';
import { reducer as formReducer } from 'redux-form';  
import users from './UserReducer';
import thunk from 'redux-thunk';


const rootReducer = combineReducers({
  places,
  users,
  form: formReducer
})

export default rootReducer;

