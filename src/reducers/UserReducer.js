import * as types from '../actions/ActionTypes';
import initialState from './InitialState';
import {browserHistory} from 'react-router';
import {combineReducers} from 'redux';
         

export default function UserReducer(state = initialState.users,  action) {  
  switch(action.type) {
    case types.LOAD_USERS_SUCCESS:
     //  return action.places;
      return action.users.map(user => Object.assign({}, user))
     return Object.assign([], state, action.users)
    case types.CREATE_USER_SUCCESS: 
    browserHistory.push('/userHome')
      return [
        ...state.filter(user => user.id !== action.user.id),
        Object.assign({}, action.user)
      ]
      case types.UPDATE_USER_SUCCESS:
      return [
        ...state.filter(user => user.id !== action.user.id),
        Object.assign({}, action.user)
      ]
      case types.LOGIN_USER_SUCCESS:
      console.log('User logged in -user reducer: ')
      console.log('user reducer: initialState: ' , initialState);
      return [
        ...state.filter(user => user.id !== action.user.id),
        Object.assign({}, state, {
          isFetching: false,
          isAuthenticated: true,
          errorMessage: ''}, action.user),
          console.log('user reducer: action.user: ' , action.user),
      ]
    case types.DELETE_USER_SUCCESS: {
      const newState = Object.assign([], state);
      const indexOfUserToDelete = state.findIndex(user => {return user.id == action.user.id})
      newState.splice(indexOfUserToDelete, 1);
      browserHistory.push('/home');
      return newState;
    }
  default:
  return state;
  }
}


