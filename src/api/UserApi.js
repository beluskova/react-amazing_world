
import { browserHistory } from 'react-router';

class UserApi {
  static getAllUsers() {
    let usersArray = value;
    return fetch('http://localhost:4000/users').then(response => {
      return response.json();
    }).catch(error => {
      return error;
    });
  }


  static getUser(user) {
    const request = new Request(`http://localhost:4000/users/${user.username}`, {
      method: 'GET',
      headers: new Headers({
        'Content-Type': 'application/json'
      }),
      body: JSON.stringify(user)
    });
    return fetch(request).then(response => {
      return response.json();
    }).catch(error => {
      return error;
    });
  }


  // -----------------------------------   CREATE USER   ------------------------------
  static createUser(newUser) {

    let users = JSON.parse(localStorage.getItem('users')) || [];
    console.log("users: " + users);
    //validate for duplication
    let duplicateUser = users.filter(user => { return user.username === newUser.username; }).length;
    if (duplicateUser) {
      window.alert('Username "' + newUser.username + '" is already taken');
      throw new Error('Incorrect username or password');
    }
    users.push(newUser);
    localStorage.setItem('users', JSON.stringify(users));

    let registeredUser = {
      id: newUser.id,
      username: newUser.username,
      firstname: newUser.firstname,
      lastname: newUser.lastname,
      token: 'fake-jwt-token',
      isAuthenticated: true
    };
    //create a token in the local storage
    localStorage.setItem('user', JSON.stringify(registeredUser));
    //save user in the database
    const request = new Request('http://localhost:4000/users', {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json'
      }),
      body: JSON.stringify(newUser)
    });
    return fetch(request).then(response => {
      return response.json();
    }).catch(error => {
      return error;
    });
  }


  //--------------------  login -----------------------------------
  static login(username, password) {
    let users = JSON.parse(localStorage.getItem('users')) || [];
    let loginUsername = username;
    let loginPassword = password;
    let filteredUsers = users.filter(user => {
      return user.username === loginUsername && user.password === loginPassword;
    });

    //administrator
    if (filteredUsers.length && loginUsername == 'admin') {

      let user = filteredUsers[0];
      let loggedinUser = {
        id: user.id,
        username: user.username,
        firstname: user.firstname,
        lastname: user.lastname,
        token: 'fake-jwt-token',
        isAuthenticated: true
      };
      localStorage.setItem('userAdmin', JSON.stringify(loggedinUser));
      browserHistory.push('/userHome');
      Promise.resolve({ ok: true, json: () => responseJson })
    }
    //regular user
    else if (filteredUsers.length) {
      let user = filteredUsers[0];
      let loggedinUser = {
        id: user.id,
        username: user.username,
        firstname: user.firstname,
        lastname: user.lastname,
        token: 'fake-jwt-token',
        isAuthenticated: true
      };
      localStorage.setItem('user', JSON.stringify(loggedinUser));
      browserHistory.push('/userHome');
      Promise.resolve({ ok: true, json: () => responseJson })
    // no match found in the users array  
    } else {
      return new Promise((resolve, reject) => {
        if (filteredUsers.length) {
          resolve({ ok: true, json: () => responseJson })
        }
        else {
          window.alert("Incorrect username or password!");
          browserHistory.push('/signup');
          reject('Username or password is incorrect');
        }
      });
    }
    return;

  }
  //---------------------------------login end-----------------------

  static logout() {
    localStorage.removeItem('user');
  }

  static logoutAdmin() {
    localStorage.removeItem('userAdmin');
  }
}
export default UserApi;

